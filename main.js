/* Bài 1: Xuất 3 số theo thứ tự tăng dần.

Input: n1, n2, n3

Step:
 + Step 1: Các trường hợp từng số bé nhất:

    - n1 bé nhất.

	n1 < n2 và n1 < n3,

 	n2 < n3 => Output = n1, n2, n3
	n2 > n3 => Output = n1, n3, n2
	n2 = n3 => Output = n1, n2, n3

	- n2 bé nhất:

	n2 < n1 và n2 < n3,

	n1 < n3 => Output = n2, n1, n3
	n1 > n3 => Output = n2, n3, n1
	n1 = n3 => Output = n2, n1, n3

	- n3 bé nhất:

	n3 < n1 và n3 < n2,

	n2 < n1 => Output = n3, n2, n1
	n2 > n1 => Output = n3, n1, n2
	n2 = n1 => Output = n3, n2, n1

 + Step 2: Các trường hợp từng số lớn nhất:

	n1 > n2 và n1 > n3 => Output = n3, n2, n1
	n2 > n3 và n2 > n1 => Output = n1, n3, n2
	n3 > n2 và n3 > n1 => Output = n1, n2, n3

 + Step 3: Trường hợp 3 số bằng nhau:

	n1 = n2 = n3 => Output = n1, n2, n3


	*/

function result_1() {
	var n1 = document.getElementById('number_1').value * 1;
	var n2 = document.getElementById('number_2').value * 1;
	var n3 = document.getElementById('number_3').value * 1;

	// Kiểm tra số nguyên.
	if (Math.ceil(n1) !== Math.floor(n1)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n2) !== Math.floor(n2)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n3) !== Math.floor(n3)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	function soft() {
		//  + Step 1: Các trường hợp từng số bé nhất:

		//  - n1 bé nhất:
		if (n1 < n2 && n1 < n3) {
			if (n2 < n3) {
				return `${n1}, ${n2}, ${n3}`;
			} else if (n2 > n3) {
				return `${n1}, ${n3}, ${n2}`;
			} else {
				return `${n1}, ${n2}, ${n3}`;
			}
		}

		//  - n2 bé nhất:
		else if (n2 < n1 && n2 < n3) {
			if (n1 < n3) {
				return `${n2}, ${n1}, ${n3}`;
			} else if (n1 > n3) {
				return `${n2}, ${n3}, ${n1}`;
			} else {
				return `${n2}, ${n1}, ${n3}`;
			}
		}

		// - n3 bé nhất:
		else if (n3 < n1 && n3 < n2) {
			if (n2 < n1) {
				return `${n3}, ${n2}, ${n1}`;
			} else if (n2 > n1) {
				return `${n3}, ${n1}, ${n2}`;
			} else {
				return `${n3}, ${n2}, ${n1}`;
			}
		}

		// Step 2: Trường hợp từng số lớn nhất.
		else {
			if (n1 > n2 && n1 > n3) {
				return `${n3}, ${n2}, ${n1}`;
			} else if (n2 > n1 && n2 > n3) {
				return `${n3}, ${n1}, ${n2}`;
			} else if (n3 > n1 && n3 > n2) {
				return `${n1}, ${n1}, ${n3}`;
			}

			// Step 3: Trường hợp 3 số bằng nhau:
			else {
				return `${n1}, ${n1}, ${n3}`;
			}
		}
	}

	var soft = soft();

	document.getElementById(
		'result_1'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png">${soft}</h5>`;
}

// Bài 2:  Chương trình "Chào hỏi".
/**
 * Input: Selector
 * Step:
 *  + Step 1: Tạo function trả vế giá trị string cho từng value selector
 * 	+ Step 2: Tạo function lấy value của các option
 *
 * Output: Xuất ra kết quả.
 */

const thanhVien = '';
const bo = 'B';
const me = 'M';
const anhTrai = 'A';
const emGai = 'E';

function chonThanhVien(member) {
	/*
	if (member == thanhVien) {
		return 'Người lạ ơi!';
	} else if (member == bo) {
		return 'Bố!';
	} else if (member == me) {
		return 'Mẹ!';
	} else if (member == anhTrai) {
		return 'Anh trai!';
	} else if (member == emGai) {
		return 'Em gái!';
	}

*/

	switch (member) {
		case thanhVien:
			return 'Người lạ ơi!';
		case bo:
			return 'Bố!';
		case me:
			return 'Mẹ!';
		case anhTrai:
			return 'Anh trai!';
		case emGai:
			return 'Em gái!';
	}
}

function result_2() {
	var mem = $('#member option:selected').val();

	document.getElementById(
		'result_2'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${chonThanhVien(
		mem
	)}</h5>`;
}

/**  Bài 3: Đếm số chẵn lẻ
 *
 * Input: Lấy value n1, n2, n3
 *
 * Step:
 * 	+ Step 1: Kiểm tra số nguyên.
 * 	+ Step 2: Kiểm tra chẵn lẻ của từng value.
 * 	+ Step 3: Đếm số chẵn lẻ.
 *
 * Output: Kết quả.
 *  */

function result_3() {
	var n1 = document.getElementById('exchange_1').value * 1;
	var n2 = document.getElementById('exchange_2').value * 1;
	var n3 = document.getElementById('exchange_3').value * 1;

	// Kiểm tra số nguyên.
	if (Math.ceil(n1) !== Math.floor(n1)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n2) !== Math.floor(n2)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(n3) !== Math.floor(n3)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	// Kiểm tra chẵn, lẻ
	// "Chẵn" = 0, "Lẻ" = 1
	var a = n1 % 2 === 0 ? 0 : 1;
	var b = n2 % 2 === 0 ? 0 : 1;
	var c = n3 % 2 === 0 ? 0 : 1;

	// Đếm số chẵn, lẻ

	var sum = a + b + c;

	// if (sum == 0) {
	// 	console.log('3 chan');
	// } else if (sum == 1) {
	// 	console.log('1 le, 2 chan');
	// } else if (sum == 2) {
	// 	console.log('2 le, 1 chan');
	// } else if (sum == 3) {
	// 	console.log('3 le');
	// }

	function soChanLe() {
		switch (sum) {
			case 0:
				return `Có 3 số chẵn, 0 số lẻ.`;
			case 1:
				return `Có 2 số chẵn, 1 số lẻ.`;
			case 2:
				return `Có 1 số chẵn, 2 số lẻ.`;
			case 3:
				return `Có 0 số chẵn, 3 số lẻ.`;
		}
	}
	var soChanLe = soChanLe();

	document.getElementById(
		'result_3'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${soChanLe}
			</h5>`;
}

// Bài 4: Đoán hình tam giác.
/**
 *
 * Input: 3 cạnh của 1 hình tam giác.
 *
 * Step:
 * 	+ Step 1: Lấy value của 3 cạnh tam giác.
 * 	+ Step 2: Kiểm tra số nguyên.
 * 	+ Step 3: Kiểm tra điều kiện để hình thành 1 tam giác.
 * 	+ Step 4: Kiểm tra điều kiện để hình thành 1 tam giác đều.
 * 	+ Step 5: Kiểm tra điều kiện để hình thành 1 tam giác cân.
 * 	+ Step 6: Kiểm tra điều kiện để hình thành 1 tam giác vuông.
 *
 *
 * Output: Xuất ra kết quả tam giác đều, cân hoặc vuông.
 */

function result_4() {
	var a = document.getElementById('height_1').value * 1;
	var b = document.getElementById('height_2').value * 1;
	var c = document.getElementById('height_3').value * 1;

	// Kiểm tra số nguyên.
	if (Math.ceil(a) !== Math.floor(a)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(b) !== Math.floor(b)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	if (Math.ceil(c) !== Math.floor(c)) {
		return alert('Nhập dữ liệu là số nguyên.');
	}

	function dkTamGiac() {
		// Điều kiện tạo thành tam giác.
		var dk_1 = a + b < c;
		var dk_2 = a + c < b;
		var dk_3 = b + c < a;

		if (dk_1 || dk_2 || dk_3) {
			return alert('Đây không phải hình tam giác');
		}

		// Điều kiện tam giác đều.
		if ((a == b) == c) {
			return 'Tam giác đều.';
		}
		// Điều kiện tam giác cân.
		if (a == b || a == c || b == c) {
			return 'Tam giác cân.';
		}

		// Diều kiện tam giác vuông.
		var dkTamGiacVuong_1 = a ^ (2 == b) ^ (2 + c) ^ 2;
		var dkTamGiacVuong_2 = b ^ (2 == a) ^ (2 + c) ^ 2;
		var dkTamGiacVuong_3 = c ^ (2 == a) ^ (2 + b) ^ 2;
		if (dkTamGiacVuong_1 || dkTamGiacVuong_2 || dkTamGiacVuong_3) {
			return 'Tam giác vuông.';
		}
	}

	var loaiTamGiac = dkTamGiac();

	document.getElementById(
		'result_4'
	).innerHTML = `<h5 class="text__result"> <img class="fish" src="https://cdn-icons-png.flaticon.com/512/3050/3050535.png"> ${loaiTamGiac}
	</h5>`;
}
